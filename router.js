var multer = require('multer');
var uploads = multer({dest:'./public/images/'});

const Authentication = require('./controllers/authentication')
const UploadController = require('./controllers/uploadcontroller')
const StreamController = require('./controllers/streamcontroller')

const passportService = require('./services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false});
const requireSignIn = passport.authenticate('local', { session: false });

module.exports = function(app){
    app.get('/', requireAuth, function(req, res){
        res.send({ message: 'Super secret code is ABC123'});
    } );
    app.post('/signin', requireSignIn, Authentication.signin);
    app.post('/signup', Authentication.signup);


    // Upload
    app.post('/api/upload', uploads.single('file'), UploadController.createFileUpload);
    app.get('/api/upload/all', requireAuth, UploadController.getAllFileUploads);

    // Access uploaded files as streams
    app.get('/api/file/:filename', StreamController.getStream);

    //app.post('/api/upload', uploads.single('mainImage'), UploadController.createFileUpload);
};