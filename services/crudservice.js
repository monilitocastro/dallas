



// obj needs Model
exports.createModel = function( req, res, next, obj ){
    const Model = obj.Model;
    const params = obj.params;
    const model = new Model(Model);
    
    for(var k in params){
        model[k] = params[k];
    }

    model.created = Date.now();
    model.save( function(err, found, numAffected){
        if(err){
            next(err);
        }

        return res.json({id: found.id});
    });
}

// obj needs Model
// TODO: test this route for future removed date
exports.getAllModels = function( req, res, next, obj ){
    const Model = obj.Model
    Model.find( { $or: [ { removed: { $eq: null } }, { removed: {$gte: Date.now() }} ] }, null, null, function(err, models){
        if(err){
            next(err);
        }

        res.json(models);
        return models;
    } );
}


// obj needs Model
exports.getModel = function( req, res, next, obj ){
    const Model = obj.Model;

    Model.findOne(obj.Params, function(err, model){
        if(err){
            // next(err);
            next({
                type: 'err',
                code: 500,
                err: err
            });
        }
        // console.log('OBJ.PARAMS: ', obj.Params)
        // console.log('MODEL: ', model)
        if(!model){
            // next(404);
            next({
                type: 'code',
                code: 404
            })
        }

        // all branches must send a code even if it is not used for consistency of the javascript object sent back to caller
        next({
            type: 'model',
            code: 200,
            model: model
        });
    });
}



// obj needs Model, Object
exports.updateModel = function( req, res, next, obj ){
    const Model = obj.Model;
    var id = req.params.id;
    if(!obj.Object){
        // return res.status(400).end('No resource to update');
        next(400);
    }

    Model.findByIdAndUpdate(id, { $set: obj.Object }, {}, function(err, model){
        if(err){
            next(err);
        }

        if(Model){
            // return res.status(200).end('Resource updated');
            next(200);
        }else{
            // return res.status(404).end('Resource not found');
            next(404);
        }
    });

}



// obj needs Model
exports.removeModel = function( req, res, next, obj ){
    const Model = obj.Model;
    var id = req.params.id;

    Model.findOneAndUpdate({_id: id, removed: {$eq: null} }, { $set: {removed: Date.now()} }, {}, function(err, model){
        if(err){
            next(err);
        }

        if(model){
            return res.status(202).end('Resource removed');
        }else{
            return res.status(404).end('Resource not found');
        }
    });
}