const FileUpload = require('../models/fileupload');
const fs = require('fs');



exports.getStream = function( req, res, next ){
    const originalname = req.params.filename;

    const Model = FileUpload;
    
    Model.findOne({originalname}, function(err, model){
        if(err){
            next(err);
        }

        if(model){
            res.setHeader("content-type", model.mimetype);
            fs.createReadStream("./public/images/" + model.filename ).pipe(res);
        }else{
            // console.log('INSIDE NOTMODEL');
            res.status(404).send({error: 'Resource does not exist'});
        }
    });
}