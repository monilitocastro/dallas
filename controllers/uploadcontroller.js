const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const FileUpload = require('../models/fileupload');

exports.createFileUpload = function( req, res, next ){
    const file = req.file; // file passed from client
    // find a description
    if(!req.body.description){
        file.description = req.body.name;
    }else{
        file.description = req.body.description;
    }
    
    const query = {originalname: file.originalname}

    FileUpload.findOne(query, function(err, doc){
        if(err){
            next(err);
        }
        if(!doc){
            doc = new FileUpload();
        }
        doc.updated = Date.now();
        for(var k in file){
            const copyAttribute = (k!=='created');
            if(copyAttribute){
                doc[k] = file[k];
            }
        }
        doc.save(function(err, result){
            if(err){
                next(err);
            }
        });
        
    });

    // fileUpload.save( function(err, found, numAffected){
    //     if(err){
    //         next(err);
    //     }

    //     return res.json({id: found.id});
    // });
}



// TODO: test this route for future removed date
exports.getAllFileUploads = function( req, res, next ){
    FileUpload.find( { $or: [ { removed: { $eq: null } }, { removed: {$gte: Date.now() }} ] }, { destination: false, filename: false, path: false }, null, function(err, models){
        if(err){
            next(err);
        }

        res.json(models);
        return models;
    } );
}

exports.getFileUpload = function( req, res, next ){
    var id = req.params.id;
    FileUpload.findById(id, function(err, model){
        if(err){
            next(err);
        }

        if(!model){
            return res.status(404).send({error: 'Resource does not exist'});
        }
        res.json(model);
    });
}

exports.updateFileUpload = function( req, res, next ){
    var id = req.params.id;
    if(!req.Object){
        return res.status(400).end('No resource to update');
    }

    FileUpload.findByIdAndUpdate(id, { $set: req.Object }, {}, function(err, model){
        if(err){
            next(err);
        }
        if(model){
            return res.status(200).end('Resource updated');
        }else{
            return res.status(404).end('Resource not found');
        }
    });
}

exports.removeFileUpload = function( req, res, next ){
    var id = req.params.id;
    
    FileUpload.findOneAndUpdate({_id: id, removed: {$eq: null} }, { $set: {removed: Date.now() } }, {}, function(err, model){
        if(err){
            next(err);
        }
        if(model){
            return res.status(202).end('Resource removed');
        }else{
            return res.status(404).end('Resource not found');
        }
    });
}