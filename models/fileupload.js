const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// define model
const fileUploadSchema = new Schema({
    
    fieldname: String,
    originalname: {
        type: String,
        unique: true
    },
    encoding: String,
    mimetype: String,
    destination: String,
    filename: String,
    path: String,
    size: Number,
    description: String,
    created: {
        type: Date,
        default: Date.now()
    },
    updated: Date,
    removed:{
        type: Date,
        default: null
    }
});


// create model class
const ModelClass = mongoose.model('fileUpload', fileUploadSchema);




//export
module.exports = ModelClass;